import os, subprocess, configparser, psutil, genres, tagpy, re
from pathlib import Path

version = 0.0
configdir = os.path.expanduser('~/.config/playmusic')

    
def getallfiles(musicdir, fixWrong=False):
    # Return a list of audio files in {musicdir}
    af = subprocess.check_output(['find', musicdir]).splitlines()
    res = []
    for bf in af:
        try:
            f = bf.decode()
            if os.path.splitext(f)[1] in [ '.mp3', '.ogg', '.flac', '.wma', 'wav' ]:
                if fixWrong and os.path.splitext(f)[1] in [ '.wma', 'wav']:
                    print ("Converting {}".format(f))
                    if convert2flac(f, f + ".flac"):
                        os.rename(f, f + ".removeme")
                        f = f + ".flac"
                res.append(f)
        except Exception as e:
            log("Bad utf-8 encoding: " + str(bf), "errors")
            continue            
    return res

def getCover(folder, default=False, album=False):
    # Return the cover image present in the folder or the default if none
    
    regexp = "(cover|folder).(jpg|png)"

    for f in os.listdir(folder):
        if re.search(regexp, f, re.IGNORECASE):
            return folder + "/" + f
    return default
    
    
def getFolderSize(folder):
    total_size = os.path.getsize(folder)
    filecount = 0 
    for item in os.listdir(folder):
        itempath = os.path.join(folder, item)
        if os.path.isfile(itempath):
            filecount = filecount + 1
            total_size += os.path.getsize(itempath)
        elif os.path.isdir(itempath):
            sz,fc = getFolderSize(itempath)
            total_size += sz
            filecount += fc
    return (total_size, filecount)


def validateTags(f):
    fref = tagpy.FileRef(f)
    tagref = fref.tag()
    if tagref.year == 0:        
        res = re.search("/\[(\d{4})\]", f)
        if res:
            year = res.group(1)
            setattr(tagref,'year',int(year))
            fref.save();
    return tagref
    
def getTags(f, quick=False):
    if quick:
        return tagpy.FileRef(f).tag()
    return validateTags(f)

def setTag(f,k,v):
    fref = tagpy.FileRef(f)
    tags = fref.tag()
    setattr(tags,k,v)
    fref.save()

    
    
def log(message, logname='messages'):
    # Log the message {MESSAGE} to playmusic logfile named {LOGNAME}.ini
    try:
        f = open("{}/{}.log".format(configdir, logname), "a")
        f.write(message + "\n")
        f.close()
        return True
    except:
        return False

def  printMiku():
    print ("""

                   . ,ε≥=`
                   |▒▒▒Q░▒▒σ
                   ▐▓╗`*▀╣╧▒≥
                   ╫▌÷ ,ó─ ▀▒╗
                   ║▀⌐#▒░   %╙▒
                   j▒╩  ▒∩ ;  -▒╦,
                   #╠░░ " └▓▌    ╙▒╬▒╖ ─
                  ╓▒▒  ∩    ▓▌       "░░░─
                ╓É░▐╙  "     ▀▌         ▒▒░
               ░▒▐▓█▓▄▄▓▓▓▓▓b ╚▀═⌐      ▐▒▒
                ▓█████▓▓▓▓▀╙            ╬▒|
    ¡╗M"      ^╓╣▒▒▀▀▀▒▒░░             ▒▒/
     '%╓,,╓≤≥╜╙.╗╨▒╙   ∩            ,≡▒╩
            ,σ²^  ▐▓▓▌ ╘▓▓▌²- .  -ⁿ⌠ë"
         .^        ▓▓▌  ▀▓▓-  ──^`
                   ▐▓▌   ▀▓▓▄
                   ▓█▓    └▀▓▓▄
                   ▐█▓      ^▓█▌
                    ██        ▀█▄
                    ▀█         ▐█▄
                    ╫█L         ██µ
                    ▓█▌        ╒█▓▌
                    ╙▀^         ^└


""")


def readConf(conf):
    try:
        cnf = configparser.ConfigParser()
        cnf.read(os.path.join(configdir, conf))
        return cnf
    except:
        return False

def readAllConfs():
    cnfs = os.listdir(configdir)
    confs = {}
    for c in cnfs:
        spl = os.path.splitext(c)
        if spl[1] == ".ini":
            cnf = readConf(c)
            if cnf:
                #print(c)
                confs[spl[0]] = cnf["settings"].get("title",spl[0])
    return confs



################################################################
# Function to manage the MOC console player                    #
################################################################

mocbin='/usr/bin/mocp'

def currentTrack():
    if not mocpRunning:
        return False
    return True

def mocpTrackInfo():
    if not mocpRunning:
        return False
    ti = subprocess.check_output([mocbin, '-i']).splitlines()
    tid = {}
    for t in ti:
        l = t.decode('utf-8').split(": ")
        tid[l[0]] = l[1]
    return tid

def mocpRunning():
    # check if the MOC server is running
    result = False
    for process in psutil.process_iter():
        if process.name() == 'mocp':
            result = True
            break
    return result

def mocpPlaying():
    ti = mocpTrackInfo()
    #print (ti)
#    print (">>" + ti["State"])
    if ti:
        if ti["State"] == 'PLAY':
            return True
    return False
    

def startMOC():
    if not mocpRunning():
            subprocess.run([mocbin, '-S'], shell=False)


def mocpAddTrack(track):
    subprocess.run([mocbin, '-a', track], shell=False)
            
def mocpPlay():
    subprocess.run([mocbin, '-p'], shell=False)

def mocpSkip():
    subprocess.run([mocbin, '-f'], shell=False)

            
def mocpToggle():
    subprocess.run([mocbin, '-G'], shell=False)


def mocpVolChange(direction, unit):
    x=subprocess.run(["amixer", "sget", "Master"])
    print ('-v{}{}'.format(direction, unit))
    subprocess.run([mocbin, '-v{}{}'.format(direction, unit)], shell=False)
    
def mocpVolDown(u):
    mocpVolChange('-', u)

def mocpVolUp(u):
    mocpVolChange('+', u)

def clearMOCplaylist():
    subprocess.run(['/usr/bin/mocp', '-c'], shell=False)

################################################################
# genres stuff                                                 @
################################################################

G = genres.GENRES
Genre = genres.Genre
G2 = {}

def direct_subgenres(g):
        if g in G and bool(G[g].subgenres):
                return G[g].subgenres
        else:
                return []

def direct_derived_forms(g):
        if g in G and bool(G[g].derived_forms):
                return G[g].derived_forms
        else:
                return []

def get_all_children(g):
        G2[g] = Genre(set(),set(),False)
        G2[g].subgenres.add(g)
        gat = list()
        dfat = list()
        for sg in direct_subgenres(g):
                gat.append(sg)
        while bool(gat):
                x = gat.pop(0)
                G2[g].add_subgenre(x)
                for sg in direct_subgenres(x):
                        gat.append(sg)
        for df in direct_derived_forms(g):
                dfat.append(df)
        while bool(dfat):
                x = dfat.pop(0)
                G2[g].add_derived_form(x)
                for df in direct_derived_forms(x):
                        dfat.append(df)

def is_ancestor(genre):
        if G[genre].alias:
                return False
        for g in G2:
                if g != genre:
                        if genre in G2[g].subgenres:
                                return False
        return True

def list_ancestors():
        for g in G2:
                if is_ancestor(g):
                        print (g)
                        
def init_dict():
        for g in G:
                get_all_children(g)

def get_genres(genre, derived=False):
    if genre in G2:
        if derived:
            return list(G2[genre].subgenres)
        else:
            return list(set().union(G2[genre].subgenres,G2[genre].derived_forms))
    else:
        return [genre]

def expandGenres(g):
    gL = set()
    for genre in g:
        for g in get_genres(genre.strip(), True):
            gL.add(g)
    return gL
    
def splitGenres(genre):
    # convert GENRE  from internal file mode (separated by ;) to a python list
    return list(map(lambda x : x.strip(), genre.split(";")))

# def addGenre(f, g):
#     userchoice = askYesNo("Add {} to all files in same directory?".format(g))
#     if userchoice == True:
#         for ff in getallfiles(os.path.dirname(f)):
#             _addGenre(ff,g)
#     elif userchoice == False:
#         _addGenre(f,g)
#     else:
#         return True
        
def addGenre(f, g):
    newg = '{};{}'.format(tagpy.FileRef(f).tag().genre, g)
    setTag(f,'genre',newg)

def removeGenre(f, g):
    setTag(f,'genre',";".join(list(filter(lambda x: not x == g, splitGenres(getTags(f).genre)))))
    
#  FFmpeg and other audio/video stuff

def convert2flac(fileIn,fileOut, params={} ):
    default = { 'loglevel':'16',
                "sample_fmt":"s16",
                "frame_rate":"320k",
                "sampling_rate":"44100",
                "filtergraph":"silenceremove=1:start_duration=1:start_threshold=-50dB:detection=peak,aformat=dblp,areverse,silenceremove=start_periods=1:start_duration=1:start_threshold=-50dB:detection=peak,aformat=dblp,areverse,volume=replaygain=track" }
    for k,v in default.items():
        if not k in params:
            params[k]=v    
    try:
        result = subprocess.run(['ffmpeg',
                                 '-loglevel', params['loglevel'],
                                 '-i', fileIn,
                                 '-sample_fmt', params['sample_fmt'],
                                 '-ac', '2',
                                 '-ab', params['frame_rate'],
                                 '-ar', params['sampling_rate'],
                                 '-af', params['filtergraph'],
                                 fileOut])
    except Exception as e:
        print("there was an error while converting {}".format(fileIn))
        print(e)
        result = False
    
    return result
