#!/usr/bin/python3

# Music Genre Diagram
# Based on dot.py
# Copyright (c) 2005 Hans Breuer <hans@breuer.org>

#    This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#  MGD-READER - a reader for the music diagram
#  Convert or use the mgd file generated from the mgd.py exporter in dia  

# mgd-reader <command> <command args...>
# <command> can be:
#  'list' for a list of all included sub-genres
#         list <genre> <include-derived-forms?>
#  'add' to add tracks of the given genre
#         add <number-of-tracks> <genre> <include-derived-forms?>

import sys, genres, random


#MPD_HOST="173.246.18.167"
MPD_HOST="192.168.1.21"
MPD_PORT=6600

G = genres.GENRES
Genre = genres.Genre
G2 = {}

def direct_subgenres(g):
        if g in G and bool(G[g].subgenres):
                return G[g].subgenres
        else:
                return []

def direct_derived_forms(g):
        if g in G and bool(G[g].derived_forms):
                return G[g].derived_forms
        else:
                return []

def get_all_children(g):
        G2[g] = Genre(set(),set(),False)
        G2[g].subgenres.add(g)
        gat = list()
        dfat = list()
        for sg in direct_subgenres(g):
                gat.append(sg)
        while bool(gat):
                x = gat.pop(0)
                G2[g].add_subgenre(x)
                for sg in direct_subgenres(x):
                        gat.append(sg)
        for df in direct_derived_forms(g):
                dfat.append(df)
        while bool(dfat):
                x = dfat.pop(0)
                G2[g].add_derived_form(x)
                for df in direct_derived_forms(x):
                        dfat.append(df)

def is_ancestor(genre):
        if G[genre].alias:
                return False
        for g in G2:
                if g != genre:
                        if genre in G2[g].subgenres:
                                return False
        return True

def list_ancestors():
        for g in G2:
                if is_ancestor(g):
                        print (g)
                        
def init_dict():
        for g in G:
                get_all_children(g)

def get_genres(genre, derived):
        if genre in G2:
                if derived is 0:
                        return list(G2[genre].subgenres)
                else:
                        return list(set().union(G2[genre].subgenres,G2[genre].derived_forms))
        else:
                return [genre]
        
def process_list_ancestors_command():
        list_ancestors()
        
def process_list_command():
        print (sys.argv)
        genre = sys.argv[2]
        derived = int(sys.argv[3])
        gl = get_genres(genre, derived)
        for g in gl:                
                 print (g)

def process_list_tracknum_command():
        genre = sys.argv[2]
        derived = int(sys.argv[3])
        gl = get_genres(genre, derived)
        mpdc = MPDClient()
        mpdc.connect(MPD_HOST, MPD_PORT)
        for g in gl:                
                print (g.ljust(30), "% 5d %5d" %(len(mpdc.list("artist", "genre", g)), len(mpdc.list("file", "genre", g))))

def process_list_artist_tracknum_command():
        genre = sys.argv[2]
        derived = int(sys.argv[3])
        gl = get_genres(genre, derived)
        mpdc = MPDClient()
        mpdc.connect(MPD_HOST, MPD_PORT)
        total_tracks = {}
        stats_artist = 0
        stats_tracks = 0
        for g in gl:
                for artist in mpdc.list("artist", "genre", g):
                        stats_artist += 1
                        tt = len(mpdc.list("file", "artist", artist, "genre", g))
                        stats_tracks += tt
                        if artist in total_tracks:
                                total_tracks[artist] += tt
                        else:
                                total_tracks[artist] =  tt
        mpdc.close()
        
        for artist in total_tracks:
                print (artist, total_tracks[artist], str(int(total_tracks[artist] * 100 / stats_tracks)) + "%")
        print ("TT: " + str(stats_tracks) + ", TA: " + str(stats_artist) + ",  average: " + str(int(stats_tracks / stats_artist)))
                
def process_list_artists_command():
        genre = sys.argv[2]
        derived = int(sys.argv[3])
        gl = get_genres(genre, derived)
        artists=set()
        mpdc = MPDClient()
        mpdc.connect(MPD_HOST, MPD_PORT)
        for g in gl:
                a = mpdc.list("artist", "genre", g)
                for aa in a:
                        artists.add(aa);
        sa = list(artists)
        sa.sort()                
        for a in sa:
                print (a)

def process_list_albums_command():
        artist = sys.argv[2]
        genre = sys.argv[3]
        derived = int(sys.argv[4])
        gl = get_genres(genre, derived)
        albums = set()
        mpdc = MPDClient()
        mpdc.connect(MPD_HOST, MPD_PORT)
        for g in gl:
                a = mpdc.list("album", "artist", artist, "genre", g)
                for aa in a:
                        albums.add(aa);
        sa = list(albums)
        sa.sort()                
        for a in sa:
                print (a)
        
                
def process_add_command():
        amount = int(sys.argv[2])
        genre = sys.argv[3]
        derived = int(sys.argv[4])
        method="end"
        gl = get_genres(genre, derived)
        mpdc = MPDClient()
        mpdc.connect(MPD_HOST, MPD_PORT)
        ts = set() 
        for g in gl:
                for t in mpdc.find("genre", g):
                        ts.add(t['file'])
        tl=list(ts)
        random.shuffle(tl)
        res = tl[0 : amount]
        if len(sys.argv) > 5:
                method = sys.argv[5]
        if method == "replace":
                status=mpdc.status()
                plsl=status["playlistlength"]
                mpdc.delete("1:"+str(plsl))
        for tta in res:
                print ("adding ", tta)
                if method == "next":
                        mpdc.addid(tta,1)
                else:
                        mpdc.add(tta)
        if method == "shuffle":
                mpdc.shuffle()
        return True

def process_dump_command():
        for x in G2:
                print (x)

                
def process_help_command():
        print ("""
 mgd-reader <command> <command args...>

 <command> can be:

  'list' for a list of all included sub-genres
         list <genre> <include-derived-forms?>

  'list_ancestors' for a list of all "ancestors" genres
         list_ancestors

  'list_artist' for a list of artists related to genre
         list_artist <genre> <include-derived-forms?>
       
  'list_tracknum' for a list of genres, with the number of tracks
         list_tracknum <genre> <include-derived-forms?>

  'add' to add tracks of the given genre
         add <number-of-tracks> <genre> <include-derived-forms?> [optional: replace|next|end|shuffle]
             where:
                replace: Delete current playlist and replace it whith the selection.
                next:    Add the selection after current track.
                end:     Add the selection at the end of the playlist.
                shuffle: Shuffle the current playlist with the selection.
  'dump'

  'help'
""")
        return True

def process_unknown_command():
        print ('ERROR: Unknown Command')
        process_help_command()
        return True

def process_error1_command():
        print ('ERROR: No Command')
        process_help_command()
        return True
        
def process_command():
        if len(sys.argv) <= 1:
                process_error1_command()
                return True
        command = sys.argv[1]
        if command == 'list':
                process_list_command()
        elif command == 'list_artists':
                process_list_artists_command()
        elif command == 'list_tracknum':
                process_list_tracknum_command()
        elif command == 'list_artist_tracknum':
                process_list_artist_tracknum_command()
        elif command == 'list_ancestors':
                process_list_ancestors_command()
        elif command == 'list_albums':
                process_list_albums_command()
        elif command == 'add':
                process_add_command()
        elif command == "help":
                process_help_command()
        elif command == "dump":
                process_dump_command()
        else:
                process_unknown_command()
        
if __name__ == '__main__':
        init_dict()
        process_command()
